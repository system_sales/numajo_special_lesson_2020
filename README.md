# 沼津情報・ビジネス専門学校～特別授業～

今回の特別授業では、BitBucketを使って、資料や成果物を管理していきます。
小まめなコミット、プッシュを心がけていきましょう。

---

## このリポジトリについて

特別授業で使用する資料やフォルダがあります。

1. **研修使用資料一式**：特別授業で使用する資料を置いています。
2. **成果物**：ソースコードや実行ファイルなど、

## コミットメッセージの書き方
コミットメッセージは以下の様に書いてください。

### ルール
> 見出し
> 
> 以下の行に詳細を書いていく

見出しの部分には先頭に以下のいずれかを書いてください。
(add)ファイル追加
(upd)ファイル更新
(del)ファイル削除

#### 書き方例
> (add)設計書作成
> 
>  要件定義の部分を記述



